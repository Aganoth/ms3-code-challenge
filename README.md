# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Code for ms3 Code Challenge

### How do I get set up? ###

The libraries and everything should be good to go. However, when I set up sqlite, I had to make a sqlite folder on my c drive, so the code sadly does have a hard coded path to where one would
store their dbs, C://sqlite//db

so, you need to create that file structure to make the code work. Apart from that, everything else is dynamic. 

Here is what the code does in summary:
File Chooser opens up, user selects their .csv
The code creates a database called test in the C://sqlite//db folder location
The code reads in the .csv file, validating entries
The code creates a stats folder where the output, a log file and a .csv file containing the
bad entries, will be stored
Output filed are created and the stats folder is populated with them.

### Contribution guidelines ###

Feed back appreciated

### Who do I talk to? ###

mitchell.hunter.hamilton@gmail.com