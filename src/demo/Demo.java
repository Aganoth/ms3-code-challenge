/*
 * Code Submitted for the ms3 code challenge
 * The user will see a JfileChooser pop up, they select the csv file
 * The code creates a folder called stats at the same level as the csv
 * stats has the log file and the csv for bad data
 */
package demo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFileChooser;

/**
 *
 * @author Mitchell
 */
public class Demo 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        //variable declarations for custom objects
        sqlHandler sHandler = new sqlHandler();
        csvHandler cHandler = new csvHandler();
        
        //ArrayLists I use to keep track of valid and invalid entries
        ArrayList<String[]> badEntries = new ArrayList<>();
        ArrayList<String[]> goodEntries = new ArrayList<>();
        
        //flag used in loop
        boolean goodEntry = true;
        
        //File chooser, the output is saved in a folder at the same level as the
        //.csv that is selected by the user.
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        int result = fileChooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) 
        {
            File selectedFile = fileChooser.getSelectedFile();
            String filePath = selectedFile.getAbsolutePath();            
            sHandler.createNewDatabase("test");
            List entries = cHandler.readFile(filePath);
            String [] columnNames = (String [])entries.get(0);
            sHandler.setColumnNames(columnNames);
            //Hard Coded the name of table, in future implementation would allow user to name it
            sHandler.createDataTable("Table_X", columnNames);
        
        //loops through all the values in the .csv file
        //Each row is represented by a String Array, we iterate through each String Array
        //Checking to see if any values are blank. If so, it is an invalid entry and stored in
        //the proper data structure
        for(int i = 1; i < entries.size(); i++)
        {
            String [] line = (String []) entries.get(i);
            for(String value: line)
            {
                if (value.equalsIgnoreCase(""))
                {
                    goodEntry = false;
                }
            }
            if(goodEntry)
            {
                goodEntries.add(line);
                goodEntry = true;
            }
            else
            {
                badEntries.add(line);
                goodEntry = true;
            }
            
        }
         sHandler.insertIntoDatabase(goodEntries, "Table_X"); 
         //Creating the folder where the output goes. We position the folder in the same 
         //Parent directory as the .csv folder. The name of the folder is stats.
         //Should probably check to make sure such a folder does not exist. Alternative
         //is to name the folder with the formatted timestamp that we use for naming the log
         //and .csv files
         File statsDir = new File(selectedFile.getParent()+"\\stats");
         statsDir.mkdir();
         String cvsFileFormat = new SimpleDateFormat("yyyyMMddHHmm'.csv'").format(new Date());
         cHandler.writeFile(statsDir+"\\bad data"+cvsFileFormat, badEntries);
         //We substract one from the entries length to acccount for the header row
         cHandler.writeLogFile(statsDir.getAbsolutePath(),entries.size()-1,goodEntries.size(),badEntries.size());
        }
    }
    
    
    
}
