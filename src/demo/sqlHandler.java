/*
 * Class for handling all Sqlite functions for the ms3 code Challenge
 */
package demo;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * 
 */
public class sqlHandler 
{
    String connectionString;
    String [] columnNames;
    
    
    public sqlHandler()
    {
        //Default Constructor
    }
    
    public void setColumnNames(String [] values)
    {
        columnNames = values;
    }
    
    //creates a new database with the parameter as the database name
    public void createNewDatabase(String fileName)
    {
        String url = "jdbc:sqlite:C:/sqlite/db/" + fileName+".db";
        connectionString  = url;
 
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                //System.out.println("The driver name is " + meta.getDriverName());
                //System.out.println("A new database has been created.");
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void createDataTable(String tableName, String [] columnNames)
    {
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS "+tableName+"(\n"
                + "	id integer PRIMARY KEY,\n";
        for(int u = 0; u < columnNames.length-1; u++)
        {
            sql = sql + "	"+columnNames[u]+" text NOT NULL,\n";
        }
        sql = sql + "	"+columnNames[columnNames.length-1]+" text NOT NULL);";       
        //System.out.println(sql);
        
        try (Connection conn = DriverManager.getConnection(connectionString);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    //Using batch execute cuts down massively on time. If you cut out the GUI part,
    //The rest of the code runs in under 10 seconds.
    public void insertIntoDatabase(ArrayList<String[]> values, String tableName)
    {
        String sql = "INSERT INTO "+tableName+"(";
        for(int u = 0; u < columnNames.length -1; u++)
        {
            sql = sql.concat(columnNames[u]+",");
        }
        sql = sql.concat(columnNames[columnNames.length -1]+") VALUES (");
        for(int u = 0; u < columnNames.length -1; u++)
        {
            sql = sql.concat("?,");
        }
        sql = sql.concat("?)");
        try(Connection conn = DriverManager.getConnection(connectionString);
                PreparedStatement pstmt = conn.prepareStatement(sql))
        {
            conn.setAutoCommit(false);
            for(int x = 0; x < values.size(); x++)
            {
                String [] line = values.get(x);
                for(int y = 0; y < line.length; y++)
                {
                    pstmt.setString(y+1,line[y]);
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            conn.commit();
            
        }
        catch (SQLException e) {
                System.out.println(e.getMessage());
            }
    }  
}
    
