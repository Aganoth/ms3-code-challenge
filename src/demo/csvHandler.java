/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mitchell
 */
public class csvHandler 
{
    
    
    public csvHandler()
    {
        //Default Constructor
    }
    
    public List readFile(String fileName) 
    {
        List myEntries = null;
        try
        {
             CSVReader reader = new CSVReader(new FileReader(fileName));
             myEntries = reader.readAll();             
        }
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(csvHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) { 
            Logger.getLogger(csvHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myEntries;
     }
    
    
    //method to write all invalid entries to a .csv file
    public void writeFile(String fileName,List badEntries)
    {
        try
        {
            CSVWriter writer = new CSVWriter(new FileWriter(fileName));
            writer.writeAll(badEntries);
            writer.close();
        } catch (IOException ex) 
        {
            Logger.getLogger(csvHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void writeLogFile(String aFolderPath, int numEntries, int numGoodEntries, int numBadEntries)
    {
        try
        {
            
            String logFileFormat = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
            File file = new File(aFolderPath+"\\"+logFileFormat);
            String logStatement = "Number of entries:"+numEntries+ " Number of good entries: "
                                  +numGoodEntries+ " Number of bad entries: "+numBadEntries;
            Files.write(file.toPath(), logStatement.getBytes(),StandardOpenOption.CREATE);
            Files.write(file.toPath(), logStatement.getBytes(),StandardOpenOption.WRITE);
        }
        catch (IOException ex)
        {
            Logger.getLogger(csvHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}   

